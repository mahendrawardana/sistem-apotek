<?php

namespace app\Http\Controllers\StokAlert;

use app\Models\mBarang;
use app\Models\mStokBarang;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use Illuminate\Support\Facades\DB;

class StokAlert extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');

        $this->menuActive = $cons['stok_alert'];
        $this->breadcrumb = [
            [
                'label' => $cons['stok_alert'],
                'route' => route('stokAlertList')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $filter_component = Main::date_filter($request, ['keywords']);
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];

        $datatable_column = [
            ["data" => "no"],
            ["data" => "brg_kode"],
            ["data" => "brg_nama"],
            ["data" => "jbr_nama"],
            ["data" => "brg_golongan"],
            ["data" => "brg_minimal_stok"],
            ["data" => "total_stok"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'keywords' => $keywords
            ),
        ]);

        return view('stokAlert/stokAlert/stokAlertList', $data);
    }


    function data_table(Request $request)
    {

        $limit = $request->input('length');
        $start = $request->input('start');
        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];
        $order_column = 'brg_kode'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $total_data_list = mBarang
            ::leftJoin('jenis_barang', 'jenis_barang.id_jenis_barang', '=', 'barang.id_jenis_barang')
            ->withCount([
                'stok_barang AS total_stok' => function ($query) {
                    $query->select(DB::raw('SUM(sbr_qty)'));
                }
            ]);

        if ($keywords) {
            $total_data_list = $total_data_list
                ->whereLike('brg_kode', $keywords)
                ->orWhereLike('brg_nama', $keywords)
                ->orWhereLike('jbr_nama', $keywords);
        }

        $total_data_list = $total_data_list->get();

        $total_data = 0;
        foreach ($total_data_list as $row) {
            if ($row->brg_minimal_stok >= $row->total_stok) {
                $total_data++;
            }
        }

        $data_list = mBarang
            ::leftJoin('jenis_barang', 'jenis_barang.id_jenis_barang', '=', 'barang.id_jenis_barang');

        if ($keywords) {
            $data_list = $data_list
                ->whereLike('brg_kode', $keywords)
                ->orWhereLike('brg_nama', $keywords)
                ->orWhereLike('jbr_nama', $keywords);
        }

        $data_list = $data_list
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $data = array();
        $t = 0;
        $no = 0;
        foreach ($data_list as $key => $row) {
            $total_stok = mStokBarang::where('id_barang', $row->id_barang)->sum('sbr_qty');
            if ($row->brg_minimal_stok >= $total_stok) {

                if ($order_type == 'asc') {
                    $no++;
                } else {
                    $no = $total_data - $t - $start;
                }

                $nestedData['no'] = $no;
                $nestedData['brg_kode'] = $row->brg_kode;
                $nestedData['brg_nama'] = $row->brg_nama;
                $nestedData['jbr_nama'] = $row->jbr_nama;
                $nestedData['brg_golongan'] = Main::barang_golongan_label($row->brg_golongan);
                $nestedData['brg_minimal_stok'] = Main::format_number($row->brg_minimal_stok);
                $nestedData['total_stok'] = Main::format_number($total_stok);


                $data[] = $nestedData;

                $t++;
            }

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
            'position' => $data_list
        );

        return $json_data;
    }

}
